

export const utils = {
    namespaced: true,
    state: {
        solMetaData: {},
    },
    getters: {
        solMetaData: (state) => state.solMetaData
    },
    mutations: {
        setSolMetaData(state, solMetaData) {
            state.solMetaData = solMetaData;
        }
    },
    actions: {
        async fetchSolMetaData({commit}) {
            let headers = {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            }
            let init = {
                method: 'GET',
                headers: headers
            }
            
            try {
                const response = await fetch("https://api.coingecko.com/api/v3/coins/solana?localization=false&tickers=false&community_data=false&developer_data=false",
                init);
                const data = await response.json();
                commit('setSolMetaData', data);
            } catch (err) {
                return console.log(err);
            }
        }
    }
}


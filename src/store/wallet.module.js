import WalletService from '../services/wallet.service';

export const wallet = {
  namespaced: true,
  state: {
    wallets: [],
    isDeleted: false,
    totalBalance: 0,
    nft : false,
    hasToken: false
  },
  getters: {
    wallets: (state) => { return state.wallets },
    getWallet: (state) => (id) => {
      return state.wallets.find(wallet => wallet.id === parseInt(id));
    }
  },
  mutations: {
    setWallet(state, wallet) {
      state.wallets.push(wallet);
    },
    setDeletedWallet(state, payload) {
      for (let i = 0; i < state.wallets.length; i++) {
        if (state.wallets[i].id === payload) {
          state.wallets.splice(i, 1);
        }
      }
    },
    setTotalBalance(state) {
      state.totalBalance = 0;
      state.wallets.forEach(wallet => {
        state.totalBalance = state.totalBalance + wallet.nativeBalance
      })
    },
    
    deleteAllWallets(state) {
      state.wallets = [];
    },

    isDeleted(state, payload) {
      state.isDeleted = payload;
    },
    getNft(state) {
      state.nft = true;
    },
    haveToken(state) {
      state.hasToken = true;
    }
  },
  actions: {
    createWallet({ commit }, payload) {
      WalletService.create(payload).then(
        response => {
          console.log(response);
          commit('setWallet', response.wallet);
          commit('setTotalBalance', response.wallet.nativeBalance)
          return response.wallet;
        },
        error => {
          console.log(error);
          commit('registerFailure');
          return error;
        }
      )
    },
    async getAllWallets({ commit }) {
      commit('deleteAllWallets');
      const response = await WalletService.getAllWallets();
      if (response) {
        response.wallets.forEach(e => {
          commit('setWallet', e);
          commit('setTotalBalance');
        });
        return await Promise.resolve(response.wallets);
      }
    },

    async deleteWallet({ commit }, payload) {
      const response = await WalletService.delete(payload);
      if (response.status === 200) {
        commit('setDeletedWallet', payload);
        commit('isDeleted', true);
        commit('setTotalBalance');
        return Promise.resolve(response);
      }
    },

    resetWallet({ commit }) {
      commit('deleteAllWallets');
    },

    async getTokenWallet({ commit }, payload) {
      const response = await WalletService.getTokenWallet(payload);
      if (response) {
        commit('setTotalBalance', response.data.nativeBalance);
        commit('haveToken');
        return Promise.resolve(response);
      }
    },

    async getNftWallet({ commit },payload) {
      const data = await WalletService.getNftWallet(payload);
      if (data) {
        commit('getNft')
        return Promise.resolve(data);
      }
    },

    async udpateWallets({ commit }) {
      const response = await WalletService.updateWallets();
      if(response) {
        response.wallets.forEach(e => {
          commit('setWallet', e);
          commit('setTotalBalance');
        });
      }
    }
  }
}
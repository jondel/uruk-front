import { createApp } from 'vue';
import App from './App.vue';

// Wallet
import SolanaWallets from 'solana-wallets-vue';
import 'solana-wallets-vue/styles.css';

// You can either import the default styles or create your own.
import 'solana-wallets-vue/styles.css';

import {
  PhantomWalletAdapter,
  SlopeWalletAdapter,
  SolflareWalletAdapter,
} from '@solana/wallet-adapter-wallets';

const walletOptions = {
  wallets: [
    new PhantomWalletAdapter(),
    new SlopeWalletAdapter(),
    new SolflareWalletAdapter({ network: 'devnet' }),
  ],
  autoConnect: true,
}

import '../public/index.css';
import router from './router';
import store from './store';
import VueApexCharts from "vue3-apexcharts";

createApp(App)
    .use(SolanaWallets, walletOptions)
    .use(store)
    .use(router)
    .use(VueApexCharts)
    .mount('#app')

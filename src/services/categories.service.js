import axios from 'axios';
import authHeader from '../services/auth-header';


class CategoriesService {

    async create(payload) {
        const dao = JSON.parse(localStorage.getItem("dao"));
        const user = JSON.parse(localStorage.getItem("user"));
        const res = await axios
            .post(process.env.VUE_APP_API_URL + "categorie/create", {
                name: payload.catName,
                daoId: dao.id,
                userId: user.id
            }, { headers: authHeader() });
        return res.data;
    }

    async getCategories() {
        const user = JSON.parse(localStorage.getItem("user"));
        const dao = JSON.parse(localStorage.getItem("dao"));
        const res = await axios
            .get(process.env.VUE_APP_API_URL + "all/categorie", {
                params: {
                    userId: user.id,
                    daoId: dao.id
                },
                headers: authHeader()
            });
        return res.data;
    }

    async deleteCategories(catId) {
        const dao = JSON.parse(localStorage.getItem("dao"));
        const user = JSON.parse(localStorage.getItem("user"));

        const res = await axios
            .post(process.env.VUE_APP_API_URL + "delete/categorie", {
                daoId: dao.id,
                userId: user.id,
                catId: catId
            }, { headers: authHeader() });
        
        return res.data
    }
}

export default new CategoriesService();
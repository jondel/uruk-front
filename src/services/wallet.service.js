import axios from 'axios';
import authHeader from '../services/auth-header';


class WalletService {
    create(wallet) {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        console.log(dao);
        return axios
            .post(process.env.VUE_APP_API_URL + "wallet/add", {
                userId: user.id,
                daoId: dao.id,
                name: wallet.walletName,
                address: wallet.walletAddress,
                nativeBalance: null
            }, { headers: authHeader() })
            .then(res => {
                return res.data;
            })
    }

    async delete(walletId) {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        console.log("deleting wallet");
        const res = await axios
            .post(process.env.VUE_APP_API_URL + "wallets/delete", {
                walletId: walletId,
                daoId: dao.id,
                userId: user.id
            }, { headers: authHeader() })
        return res;

    }

    async getAllWallets() {
        const daoUser = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        if (daoUser) {
            const res = await axios
                .get(process.env.VUE_APP_API_URL + "wallets/", {
                    params: {
                        userId: user.id,
                        daoId: daoUser.id
                    },
                    headers: authHeader()
                });
            return res.data;
        } else {
            return;
        }
    }

    async getTokenWallet(address) {
        const daoUser = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        if (daoUser) {
            const res = await axios
                .post(process.env.VUE_APP_API_URL + "getwallettoken", {
                    userId: user.id,
                    daoId: daoUser.id,
                    address: address,
                }, { headers: authHeader() })
            return res
        } else {
            return;
        }
    }

    async getNftWallet(address) {
        const daoUser = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        if (daoUser) {
            const res = await axios
                .get(process.env.VUE_APP_API_URL + "nft", {
                    params: {
                        userId: user.id,
                        daoId: daoUser.id,
                        address: address
                    },
                    headers: authHeader()
                })
            return res
        } else {
            return;
        }
    }

    async updateWallets() {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        if (dao) {
            const { data } = await axios.post(process.env.VUE_APP_API_URL + "update", {
                daoId: dao.id,
                userId: user.id,
            }, { headers: authHeader() })

            return data;
        }
    }
}

export default new WalletService();
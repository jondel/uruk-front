import axios from 'axios';
import authHeader from '../services/auth-header';

class DaoService {
    create(daoName) {
        const user = JSON.parse(localStorage.getItem('user'));
        console.log(user);
        return axios
            .post(process.env.VUE_APP_API_URL + "dao/create", {
                userId: user.id,
                name: daoName.daoName
            }, { headers: authHeader() })
            .then(res => {
                localStorage.setItem('dao', JSON.stringify(res.data.dao));
                return res.data;
            })
    }
    async delete() {
        const user = JSON.parse(localStorage.getItem('user'));
        const dao = JSON.parse(localStorage.getItem('dao'));
        const { data } = await axios.post(process.env.VUE_APP_API_URL + "delete/dao", {
            userId: user.id,
            daoId: dao.id
        }, { headers: authHeader() })
        localStorage.removeItem('dao');
        localStorage.removeItem('user');

        return data;
    }
    async updateImage(files) {
        const fileUpload = files[0]
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        const { data } = await axios
            .post(process.env.VUE_APP_API_URL + "image/upload", {
                fileUpload,
                daoId: dao.id
            }, { headers: {
                'Content-Type': 'multipart/form-data',
                "Accept": "*/*",
                'x-access-token': user.accessToken
            } });
        return data;
    }
    async updateDaoName(payload) {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        const { data } = await axios.post(process.env.VUE_APP_API_URL + "update/dao",{
            daoId: dao.id,
            userId: user.id,
            name: payload.name
        }, { headers: authHeader() });
        return data;
    }

    async createPdf(payload) {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        const { data } = await axios.post(process.env.VUE_APP_API_URL + "pdf/create", {
            daoId: dao.id,
            userId: user.id,
            pdfName: payload.name
        }, { headers: authHeader() });

        return data;
    }

    async deletePdf(payload) {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        const { data } = await axios.post(process.env.VUE_APP_API_URL + "delete/pdf", {
            daoId: dao.id,
            userId: user.id,
            pdfUrl: payload.value
        }, { headers: authHeader() });

        return data;
    }
}

export default new DaoService();

import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import Dashboard from "../views/DashboardView.vue"
// component vue
import AddWallet from "../components/AddWallet.vue";
import MyWallets from "../components/wallets/MyWallets.vue";
import OneWallet from "../components/wallets/OneWallet.vue";
import Transactions from "../components/TransactionsData.vue"
import Setting from "../components/settings/SettingsIndex.vue";
import DaoDashboard from "../components/dashboard/DaoDashboard.vue";
import AdminDashboard from "../components/dashboard/AdminDashboard.vue";
import UserDashboard from "../components/UserDashboard.vue";
import TheCategories from "../components/TheCategories.vue";

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    children: [
      {
        path: '/addwallet',
        name: 'addwallet',
        component: AddWallet
      },
      {
        path: '/settings',
        name: 'settings',
        component: Setting
      },
      {
        path: '/transactions/:address',
        name: 'transactions',
        component: Transactions
      },
      {
        path: '/daoowner',
        name: 'Dao Dashboard',
        component: DaoDashboard
      },
      {
        path: '/user',
        name: 'userDashboard',
        component: UserDashboard
      },
      {
        path: '/admin-dashboard',
        name: 'adminDashboard',
        component: AdminDashboard
      },
      {
        path: '/mywallets',
        name: 'mywallets',
        component: MyWallets
      },
      {
        path: '/onewallet/:id',
        name: 'onewallet',
        component: OneWallet
      },
      {
        path: '/categories',
        name: 'My Categories',
        component: TheCategories
      }
    ]
  },
  
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "register" */ '../components/auth/TheRegister.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

 router.beforeEach((to, from, next) => {
  document.title = to.name;
  const publicPages = ['/login', '/register', '/'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');
   //trying to access a restricted page + not logged in
   //redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});

export default router
